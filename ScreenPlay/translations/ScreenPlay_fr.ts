<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>Community</name>
    <message>
        <source>We use Stomt because it provides quick and easy feedback via I like/I wish. So you can easily give us feedback and speak your mind. We will read these wishes on a daily basis!</source>
        <translation type="obsolete">Nous utilisons Stomt parce qu&apos;il fournit un feedback rapide et facile via J&apos;aime / Je souhaite. Vous pouvez donc facilement nous donner votre avis et nous dire ce que vous pensez. Nous lirons ces souhaits quotidiennement !</translation>
    </message>
    <message>
        <source>Open ScreenPlay Stomt page</source>
        <translation type="obsolete">Ouvrir la page ScreenPlay Stomt</translation>
    </message>
    <message>
        <source>Forums</source>
        <translation type="vanished">Forum</translation>
    </message>
    <message>
        <source>Blog</source>
        <translation type="obsolete">Blog</translation>
    </message>
    <message>
        <source>Source Code</source>
        <translation type="obsolete">Code source</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="obsolete">Workshop</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="34"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="40"/>
        <source>Forum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="45"/>
        <source>Issue List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="50"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="55"/>
        <source>Contribution Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="60"/>
        <source>Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <location filename="../qml/Community/CommunityNavItem.qml" line="59"/>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Create</name>
    <message>
        <location filename="../qml/Create/Create.qml" line="101"/>
        <source>Create wallpapers and widgets for local usage or the steam workshop!</source>
        <translation type="unfinished">Créez des fonds d&apos;écran et des widgets pour une utilisation locale ou pour l&apos;atelier de vapeur !</translation>
    </message>
</context>
<context>
    <name>CreateContent</name>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="15"/>
        <source>Create Widgets and Scenes</source>
        <translation type="unfinished">Créer des widgets et des scènes</translation>
    </message>
    <message>
        <source>Create Emtpy Widget</source>
        <translation type="obsolete">Créer un widget vide</translation>
    </message>
    <message>
        <source>Examples Widgets and Scenes</source>
        <translation type="obsolete">Exemples de Widgets et de Scènes</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="29"/>
        <source>Create Empty Widget</source>
        <translation type="unfinished">Créer un widget vide</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="42"/>
        <source>Example Widgets and Scenes</source>
        <translation type="unfinished">Exemples de Widgets et de Scène</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="65"/>
        <source>Empty HTML Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple clock widget</source>
        <translation type="obsolete">Widget horloge simple</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="75"/>
        <source>Musik scene wallpaper visualizer</source>
        <translation type="unfinished">Visualiseur de fond d&apos;écran de scène musicale</translation>
    </message>
    <message>
        <location filename="../qml/Create/CreateContent.qml" line="86"/>
        <source>Changing scene wallpaper via unsplash.com</source>
        <translation type="unfinished">Changement de fond d&apos;écran via unsplash.com</translation>
    </message>
</context>
<context>
    <name>CreateContentButton</name>
    <message>
        <location filename="../qml/Create/CreateContentButton.qml" line="114"/>
        <source>Not yet implemented. Stay tuned!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyHtmlWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="54"/>
        <source>This wizard lets you create a empty html based wallpaper. You can put anything you can imagine into this html file. For example this can be a three.js scene or a utility application written in javascript.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="68"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="119"/>
        <source>Create a html Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="123"/>
        <source>General</source>
        <translation type="unfinished">Généralités</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="131"/>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="145"/>
        <source>Copyright owner</source>
        <translation type="unfinished">Titulaire du droit d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="149"/>
        <source>License</source>
        <translation type="unfinished">Licence</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="172"/>
        <source>Tags</source>
        <translation type="unfinished">Tags</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="184"/>
        <source>Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="191"/>
        <source>You can set your own preview image here!</source>
        <translation type="unfinished">Vous pouvez définir votre propre image de prévisualisation ici !</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="216"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="228"/>
        <source>Save</source>
        <translation type="unfinished">Sauvegarder</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyHtmlWallpaper/CreateEmptyHtmlWallpaper.qml" line="263"/>
        <source>Create Html Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateEmptyWidget</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="17"/>
        <source>Create an empty widget</source>
        <translation type="unfinished">Créer un widget vide</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="77"/>
        <source>General</source>
        <translation type="unfinished">Généralités</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="85"/>
        <source>Widget name</source>
        <translation type="unfinished">Nom du widget</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="98"/>
        <source>Copyright owner</source>
        <translation type="unfinished">Titulaire du droit d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="102"/>
        <source>Type</source>
        <translation type="unfinished">Tapez</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="131"/>
        <source>License</source>
        <translation type="unfinished">Licence</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="154"/>
        <source>Tags</source>
        <translation type="unfinished">Tags</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="178"/>
        <source>Save</source>
        <translation type="unfinished">Sauvegarder</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="205"/>
        <source>Abort</source>
        <translation type="unfinished">Abandonner</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateEmptyWidget/CreateEmptyWidget.qml" line="233"/>
        <source>Create Widget...</source>
        <translation type="unfinished">Créer un widget...</translation>
    </message>
</context>
<context>
    <name>CreateImport</name>
    <message>
        <source>Import a Creation</source>
        <translation type="vanished">Importer une création</translation>
    </message>
</context>
<context>
    <name>CreateUpload</name>
    <message>
        <source>Upload a Creation</source>
        <translation type="vanished">Poster une création</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperCodec</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="52"/>
        <source>Import a video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="62"/>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="76"/>
        <source>Set your preffered video codec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="108"/>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperCodec.qml" line="125"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="18"/>
        <source>An error occurred!</source>
        <translation type="unfinished">Une erreur s&apos;est produite !</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="78"/>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Copier le texte dans le presse-papiers</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperResult.qml" line="88"/>
        <source>Back to create and send an error report!</source>
        <translation type="unfinished">Retour pour créer et envoyer un rapport d&apos;erreur !</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="54"/>
        <source>Generating preview image...</source>
        <translation type="unfinished">Génération d&apos;une image de prévisualisation...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="57"/>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Génération d&apos;une image miniature de prévisualisation...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="64"/>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Génération d&apos;une vidéo de prévisualisation de 5 secondes...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="67"/>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Génération d&apos;un aperçu gif...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="76"/>
        <source>Converting Audio...</source>
        <translation type="unfinished">Conversion audio...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="79"/>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Conversion de la vidéo... Cela peut prendre un certain temps !</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="83"/>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Conversion de la vidéo ERREUR !</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="86"/>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="114"/>
        <source>Convert a video to a wallpaper</source>
        <translation type="unfinished">Convertir une vidéo en fond d&apos;écran en direct</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="164"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="211"/>
        <source>Generating preview video...</source>
        <translation type="unfinished">Génération d&apos;une vidéo de prévisualisation...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="224"/>
        <source>You can set your own preview image here!</source>
        <translation type="unfinished">Vous pouvez définir votre propre image de prévisualisation ici !</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="258"/>
        <source>Name (required!)</source>
        <translation type="unfinished">Nom (obligatoire !)</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="273"/>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="281"/>
        <source>Youtube URL</source>
        <translation type="unfinished">URL Youtube</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="309"/>
        <source>Abort</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="322"/>
        <source>Save</source>
        <translation type="unfinished">Sauvegarder</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/CreateWallpaper/CreateWallpaperVideoImportConvert.qml" line="359"/>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Sauvegarder le fond d´écran...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="41"/>
        <source>Volume</source>
        <translation type="unfinished">Volume</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="55"/>
        <source>Playback rate</source>
        <translation type="unfinished">Taux de lecture</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="67"/>
        <source>Current Video Time</source>
        <translation type="unfinished">Temps vidéo actuel</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="85"/>
        <source>Fill Mode</source>
        <translation type="unfinished">Mode de remplissage</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="110"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="113"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="116"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="119"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="122"/>
        <source>Scale_Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FFMPEGPopup</name>
    <message>
        <source>Begin downloading FFMPEG</source>
        <translation type="obsolete">Commencez à télécharger le FFMPEG</translation>
    </message>
    <message>
        <source>FFMPEG download failed</source>
        <translation type="obsolete">Le téléchargement du FFMPEG a échoué</translation>
    </message>
    <message>
        <source>FFMPEG download successful</source>
        <translation type="obsolete">Téléchargement FFMPEG réussi</translation>
    </message>
    <message>
        <source>Extracting FFMPEG</source>
        <translation type="obsolete">Extraction de la FFMPEG</translation>
    </message>
    <message>
        <source>ERROR extracting ffmpeg from RAM</source>
        <translation type="obsolete">ERROR extrayendo ffmpeg de la RAM</translation>
    </message>
    <message>
        <source>ERROR extracing ffmpeg</source>
        <translation type="obsolete">ERREUR extraire ffmpeg</translation>
    </message>
    <message>
        <source>ERROR saving FFMPEG to disk</source>
        <translation type="obsolete">ERREUR enregistrement de FFMPEG sur disque</translation>
    </message>
    <message>
        <source>ERROR extracing FFPROBE</source>
        <translation type="obsolete">ERREUR d&apos;extraction FFPROBE</translation>
    </message>
    <message>
        <source>ERROR saving FFPROBE to disk</source>
        <translation type="obsolete">ERREUR enregistrement de FFPROBE sur le disque</translation>
    </message>
    <message>
        <source>Extraction successful</source>
        <translation type="obsolete">Succès de l´extraction</translation>
    </message>
    <message>
        <source>All done and ready to go!</source>
        <translation type="obsolete">Tout est fait et prêt à partir !</translation>
    </message>
    <message>
        <source>You cannot create Wallpaper without FFMPEG installed!</source>
        <translation type="obsolete">Vous ne pouvez pas créer de fonds d´écrans sans FFMPEG installé !</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>Download FFMPEG</source>
        <translation type="obsolete">Télécharger FFMPEG</translation>
    </message>
    <message>
        <source>Before we can start creating content you need to download FFMPEG</source>
        <translation type="obsolete">Avant que nous puissions commencer à créer du contenu, vous devez télécharger FFMPEG</translation>
    </message>
    <message>
        <source>&lt;b&gt;Why do we bother you with this?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Well its because of &lt;b&gt;copyright&lt;/b&gt; and many many &lt;b&gt;patents&lt;/b&gt;.
Files like .mp4 or .webm are containers for video and audio. Every audio
and video file is encoded with a certain codec. These can be open source
ceand free to use like &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; and the newer  &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (the one YouTube uses for their web
ms)but there are also some proprietary ones like  &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; and the successor &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 We as software developer now need to deal with stuff like this in a field wedo not have any expertise in. The desicion to enable only free codecs for content wasan easy one but we still need to provide a way for our user to import wallpaper without a hassle. We do not provide  &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; for converting video and audio with ScreenPlay because we are not allowed to. We let the user download &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; wich is perfectly fine!
&lt;br&gt;
Sorry for this little inconvenience :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
IF YOU DO NOT HAVE A INTERNET CONNECT YOU CAN SIMPLY PUT FFMPEG AND FFPROBE
IN THE SAME FOLDER AS YOUR SCREENPLAY EXECUTABLE!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
This is usually:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
if you installed ScreenPlay via Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</source>
        <translation type="obsolete">&lt;b&gt;Pourquoi on vous dérange avec ça ? &lt;/b&gt;
&lt;br&gt;&lt;br&gt; Eh bien c&apos;est à cause de &lt;b&gt;copyright&lt;/b&gt; et de beaucoup de &lt;b&gt;brevets&lt;/b&gt;.
Les fichiers comme .mp4 ou .webm sont des conteneurs pour la vidéo et l&apos;audio. Chaque audio
et le fichier vidéo est encodé avec un certain codec. Ceux-ci peuvent être open source
et libre à utiliser comme &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; et le plus récent &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (celui que YouTube utilise pour son site web
ms)mais il y a aussi des propriétaires comme &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; et le successeur &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 En tant que développeur de logiciels, nous devons maintenant nous occuper de ce genre de choses dans un domaine dans lequel nous n&apos;avons aucune expertise. La décision de n&apos;autoriser que les codecs libres pour le contenu a été facile à prendre, mais nous devons encore fournir à nos utilisateurs un moyen d&apos;importer des fonds d&apos;écran sans problème. Nous ne fournissons pas de &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; pour la conversion vidéo et audio avec ScreenPlay car nous n&apos;en avons pas le droit. Nous laissons l&apos;utilisateur télécharger &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; ce qui est parfaitement correct !
&lt;br&gt;
Désolé pour ce petit inconvénient :)
&lt;br&gt;
&lt;br&gt;
&lt;centre&gt;
&lt;b&gt;
SI VOUS N&apos;AVEZ PAS DE CONNEXION INTERNET, VOUS POUVEZ SIMPLEMENT METTRE FFMPEG ET FFPROBE
DANS LE MÊME DOSSIER QUE L&apos;EXÉCUTABLE DE VOTRE SCÉNARIO !
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
C&apos;est généralement le cas :
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
si vous avez installé ScreenPlay via Steam !
&lt;/centre&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Why do we bother you with this?&lt;/b&gt;
&lt;br&gt;&lt;br&gt; Well its because of &lt;b&gt;copyright&lt;/b&gt; and many many &lt;b&gt;patents&lt;/b&gt;.
Files like .mp4 or .webm are containers for video and audio. Every audio
and video file is encoded with a certain codec. These can be open sour
ceand free to use like &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; and the newer  &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (the one YouTube uses for their web
ms)but there are also some proproatary ones like  &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; and the successor &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 We as software developer now need to deal with stuff like this in a field we
are do not have any expertise in. The desicion to enable only free codecs for content was
an easy one but we still need to provide a way for our user to import wallpape
r without a hassle. We do not provide  &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; f
or converting video and audio with ScreenPlay because we are not allowed to. We let the user download &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; wich is perfectly fine!
&lt;br&gt;
Sorry for this little inconvenience :)
&lt;br&gt;
&lt;br&gt;
&lt;center&gt;
&lt;b&gt;
IF YOU DO NOT HAVE A INTERNET CONNECT YOU CAN SIMPLY PUT FFMPEG AND FFPROBE
IN THE SAME FOLDER AS YOUR SCREENPLAY EXECUTABLE!
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
This is usually:
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
if you installed ScreenPlay via Steam!
&lt;/center&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</source>
        <translation type="obsolete">&lt;b&gt;Pourquoi on vous dérange avec ça ? &lt;/b&gt;
&lt;br&gt;&lt;br&gt; Eh bien c&apos;est à cause de &lt;b&gt;copyright&lt;/b&gt; et de beaucoup de &lt;b&gt;brevets&lt;/b&gt;.
Les fichiers comme .mp4 ou .webm sont des conteneurs pour la vidéo et l&apos;audio. Chaque audio
et le fichier vidéo est encodé avec un certain codec. Ces derniers peuvent être open sour
ceet libre à utiliser comme &lt;a href=&apos;https://wikipedia.org/wiki/VP8&apos;&gt;VP8&lt;/a&gt; et le plus récent &lt;a href=&apos;https://wikipedia.org/wiki/VP9&apos;&gt;VP9&lt;/a&gt; (celui que YouTube utilise pour leur site web
ms)mais il y a aussi des proproataries comme &lt;a href=&apos;https://wikipedia.org/wiki/H.264/MPEG-4_AVC&apos;&gt;h264&lt;/a&gt; et le successeur &lt;a href=&apos;https://wikipedia.org/wiki/High_Efficiency_Video_Coding&apos;&gt;h265&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;


 En tant que développeur de logiciels, nous devons maintenant nous occuper de ce genre de choses dans un domaine
n&apos;ont pas d&apos;expertise dans. La décision de n&apos;autoriser que les codecs libres pour le contenu a été
facile, mais nous devons quand même fournir un moyen pour notre utilisateur d&apos;importer wallpape
r sans tracas. Nous ne fournissons pas &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; f
ou la conversion vidéo et audio avec ScreenPlay car nous n&apos;en avons pas le droit. Nous laissons l&apos;utilisateur télécharger &lt;a href=&apos;https://ffmpeg.org/&apos;&gt;FFMPEG&lt;/a&gt; ce qui est parfaitement bien !
&lt;br&gt;
Désolé pour ce petit inconvénient :)
&lt;br&gt;
&lt;br&gt;
&lt;centre&gt;
&lt;b&gt;
SI VOUS N&apos;AVEZ PAS DE CONNEXION INTERNET, VOUS POUVEZ SIMPLEMENT METTRE FFMPEG ET FFPROBE
DANS LE MÊME DOSSIER QUE L&apos;EXÉCUTABLE DE VOTRE SCÉNARIO !
&lt;/b&gt;
&lt;br&gt;
&lt;br&gt;
C&apos;est généralement le cas :
&lt;br&gt; C:Program Files (x86)SteamsteamappscommonScreenPlay
&lt;br&gt;
si vous avez installé ScreenPlay via Steam !
&lt;/centre&gt;
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;~ Kelteseth | Elias Steurer&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Not now!</source>
        <translation type="obsolete">Pas maintenant !</translation>
    </message>
    <message>
        <source>You can now start creating content!</source>
        <translation type="obsolete">Vous pouvez maintenant commencer à créer du contenu !</translation>
    </message>
    <message>
        <source>Start!</source>
        <translation type="obsolete">Lancement !</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Create/Footer.qml" line="23"/>
        <source>QML Quickstart Guide</source>
        <translation type="unfinished">Guide de démarrage rapide QML</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="34"/>
        <source>Documentation</source>
        <translation type="unfinished">Documentation</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="45"/>
        <source>Forums</source>
        <translation type="unfinished">Forum</translation>
    </message>
    <message>
        <location filename="../qml/Create/Footer.qml" line="56"/>
        <source>Workshop</source>
        <translation type="unfinished">Workshop</translation>
    </message>
</context>
<context>
    <name>Headline</name>
    <message>
        <location filename="../qml/Common/Headline.qml" line="16"/>
        <source>Headline</source>
        <translation type="unfinished">Titre</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="142"/>
        <source>Clear</source>
        <translation type="unfinished">Effacer</translation>
    </message>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="160"/>
        <source>Select Preview Image</source>
        <translation type="unfinished">Sélectionnez l&apos;image de prévisualisation</translation>
    </message>
</context>
<context>
    <name>ImportContent</name>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="22"/>
        <source>Import Content</source>
        <translation type="unfinished">Contenu de l&apos;importation</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="75"/>
        <source>Import video</source>
        <translation type="unfinished">Importer une vidéo</translation>
    </message>
    <message>
        <source>FFMPEG Needed for import</source>
        <translation type="obsolete">FFMPEG Nécessaire à l&apos;importation</translation>
    </message>
    <message>
        <location filename="../qml/Create/ImportContent.qml" line="161"/>
        <source>Upload Exsisting Project to Steam</source>
        <translation type="unfinished">Télécharger un projet existant vers Steam</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="99"/>
        <source>Refreshing!</source>
        <translation type="unfinished">Rafraîchissant !</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="102"/>
        <location filename="../qml/Installed/Installed.qml" line="124"/>
        <source>Pull to refresh!</source>
        <translation type="unfinished">Tirez pour vous rafraîchir !</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="141"/>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation type="unfinished">Obtenez plus de Wallpaper &amp; Widgets via l&apos;atelier Steam!</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="210"/>
        <source>Open containing folder</source>
        <translation type="unfinished">Ouvrir le dossier contenant</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="218"/>
        <source>Deinstall Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="227"/>
        <source>Open workshop Page</source>
        <translation type="unfinished">Ouvrir la page de l&apos;atelier</translation>
    </message>
    <message>
        <source>All</source>
        <translation type="obsolete">Tout</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="obsolete">Vidéos</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="obsolete">Scènes</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="obsolete">Widgets</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="329"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="obsolete">Búsqueda de Fondos y Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="54"/>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation type="unfinished">Obtenez des widgets et des fonds d&apos;écran gratuits via Steam</translation>
    </message>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="84"/>
        <source>Browse the Steam Workshop</source>
        <translation type="unfinished">Parcourir l&apos;atelier Steam</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="67"/>
        <source>Wallpaper Configuration</source>
        <translation type="unfinished">Configuration du fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="132"/>
        <source>Remove selected</source>
        <translation type="unfinished">Supprimer la sélection</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="144"/>
        <location filename="../qml/Monitors/Monitors.qml" line="158"/>
        <source>Remove </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="145"/>
        <source>Wallpapers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="159"/>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
    <message>
        <source>Remove all Wallpapers</source>
        <translation type="obsolete">Supprimer tous les fonds d&apos;écran</translation>
    </message>
    <message>
        <source>Remove all Widgets</source>
        <translation type="obsolete">Supprimer tous les Widgets</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="132"/>
        <source>Set color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="155"/>
        <source>Please choose a color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="57"/>
        <source>All</source>
        <translation type="unfinished">Tout</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="73"/>
        <source>Scenes</source>
        <translation type="unfinished">Scènes</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="89"/>
        <source>Videos</source>
        <translation type="unfinished">Vidéos</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="105"/>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="46"/>
        <source> Subscribed items: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="87"/>
        <source>Upload to the Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="70"/>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation type="unfinished">Configurer un fond d&apos;écran ou des widgets actifs</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="72"/>
        <source>No active Wallpaper or Widgets</source>
        <translation type="unfinished">Pas de fond d&apos;écran ou de widgets actifs</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="28"/>
        <source>You need to run Steam for this :)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="37"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="22"/>
        <source>Abort Upload.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="30"/>
        <source>I Agree to the Steam Workshop Agreement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <location filename="../qml/Monitors/SaveNotification.qml" line="40"/>
        <source>Profile saved successfully!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>Open containing folder</source>
        <translation type="obsolete">Ouvrir le dossier contenant</translation>
    </message>
    <message>
        <source>Open workshop Page</source>
        <translation type="obsolete">Ouvrir la page de l&apos;atelier</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/Common/Search.qml" line="45"/>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="unfinished">Búsqueda de Fondos y Widgets</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="59"/>
        <source>General</source>
        <translation type="unfinished">Généralités</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="74"/>
        <source>Autostart</source>
        <translation type="unfinished">Démarrage automatique</translation>
    </message>
    <message>
        <source>ScreenPlay will start with your operating system and will setup your Desktop every time for you.</source>
        <translation type="obsolete">ScreenPlay démarrera avec votre système d´exploitation et configurera votre bureau à chaque fois pour vous.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="83"/>
        <source>High priority Autostart</source>
        <translation type="unfinished">Démarrage automatique en haute priorité</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="86"/>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation type="unfinished">Cette option accorde à ScreenPlay une priorité de démarrage automatique supérieure à celle des autres applications.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="95"/>
        <source>Send anonymous crash reports and statistics</source>
        <translation type="unfinished">Envoyer des rapports d&apos;accident et des statistiques anonymes</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="96"/>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes!</source>
        <translation type="unfinished">Aidez-nous à rendre ScreenPlay plus rapide et plus stable. Toutes les données collectées sont purement anonymes et utilisées uniquement à des fins de développement !</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="105"/>
        <source>Set save location</source>
        <translation type="unfinished">Définir l&apos;emplacement de sauvegarde</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="116"/>
        <source>Set location</source>
        <translation type="unfinished">Définir l&apos;emplacement</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="131"/>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation type="unfinished">Important : La modification de ce répertoire n&apos;a aucun effet sur le chemin de téléchargement de l&apos;atelier. ScreenPlay ne supporte qu&apos;un seul dossier de contenu !</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="149"/>
        <source>Language</source>
        <translation type="unfinished">Langue</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="150"/>
        <source>Set the ScreenPlay UI Language</source>
        <translation type="unfinished">Définir la langue de l&apos;interface utilisateur ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="171"/>
        <source>Chinese - Simplified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="225"/>
        <source>Performance</source>
        <translation type="unfinished">Performance</translation>
    </message>
    <message>
        <source>Pause wallpaper while ingame</source>
        <translation type="obsolete">Met en pause le fond d´écran pendant le jeu</translation>
    </message>
    <message>
        <source>To maximise your framerates ingame, you can enable this setting to pause all active wallpapers!</source>
        <translation type="obsolete">Pour maximiser vos framerates en jeu, vous pouvez activer ce paramètre pour mettre en pause tous les wallpapers actifs !</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="75"/>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="110"/>
        <source>Your storage path is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="165"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="168"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="174"/>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="177"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="180"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="183"/>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="186"/>
        <source>Vietnamese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="194"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="195"/>
        <source>Switch dark/light theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="209"/>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="212"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="215"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="240"/>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="241"/>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="251"/>
        <source>Default Fill Mode</source>
        <translation type="unfinished">Mode de remplissage par défaut</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="252"/>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation type="unfinished">Définissez cette propriété pour définir la manière dont la vidéo est mise à l&apos;échelle pour s&apos;adapter à la zone cible.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="264"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="267"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="270"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="273"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="276"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="286"/>
        <source>About</source>
        <translation type="unfinished">À propos de</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="314"/>
        <source>Thank you for using ScreenPlay</source>
        <translation type="unfinished">Merci d&apos;utiliser ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="329"/>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation type="unfinished">Bonjour, je suis Elias Steurer, aussi connu sous le nom de Kelteseth, et je suis le développeur de ScreenPlay. Merci d&apos;utiliser mon logiciel. Vous pouvez me suivre pour recevoir des mises à jour sur ScreenPlay ici :</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="418"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="419"/>
        <source>ScreenPlay Build Version </source>
        <translation type="unfinished">Version de construction de ScreenPlay </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="421"/>
        <source>Open Changelog</source>
        <translation type="unfinished">Ouvrir le journal des modifications</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="428"/>
        <source>Third Party Software</source>
        <translation type="unfinished">Logiciel tiers</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="429"/>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation type="unfinished">ScreenPlay ne serait pas possible sans le travail des autres. Un grand merci à : </translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="430"/>
        <source>Licenses</source>
        <translation type="unfinished">Licences</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="448"/>
        <location filename="../qml/Settings/Settings.qml" line="450"/>
        <source>Debug Messages</source>
        <translation type="unfinished">Messages de débogage</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="449"/>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation type="unfinished">Si votre ScreenPlay se comporte mal, c&apos;est une bonne façon de chercher des réponses. Ceci montre tous les journaux et les avertissements pendant l&apos;exécution.</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="461"/>
        <source>Data Protection</source>
        <translation type="unfinished">Protection des données</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="462"/>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation type="unfinished">Nous utilisons vos données avec beaucoup de soin pour améliorer le ScreenPlay. Nous ne vendons ni ne partageons ces informations (anonymes) avec d&apos;autres personnes !</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="463"/>
        <source>Privacy</source>
        <translation type="unfinished">Confidentialité</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <location filename="../qml/Settings/SettingsExpander.qml" line="51"/>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Copier le texte dans le presse-papiers</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="99"/>
        <source>Set Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="208"/>
        <source>Headline</source>
        <translation type="unfinished">Titre</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="259"/>
        <source>Select a Monitor to display the content</source>
        <translation type="unfinished">Sélectionnez un moniteur pour afficher le contenu</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="294"/>
        <source>Set Volume</source>
        <translation type="unfinished">Régler le volume audio</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="304"/>
        <source>Fill Mode</source>
        <translation type="unfinished">Mode de remplissage</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="327"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="330"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="333"/>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="336"/>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="339"/>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set wallpaper</source>
        <translation type="obsolete">Configurer le wallpaper</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="96"/>
        <source>Set Wallpaper</source>
        <translation type="unfinished">Configurer le wallpaper</translation>
    </message>
    <message>
        <source>Create Widget</source>
        <translation type="obsolete">Configurer le widget</translation>
    </message>
    <message>
        <source>Create Wallpaper</source>
        <translation type="obsolete">Créer un wallpaper</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="89"/>
        <source>Project size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="89"/>
        <source> MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="95"/>
        <source>No description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="237"/>
        <source>Click here if you like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="251"/>
        <source>Click here if you do not like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="286"/>
        <source>Tags: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="311"/>
        <source>Subscribtions: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="317"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="360"/>
        <source>Subscribed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="360"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="12"/>
        <source>Save</source>
        <translation type="unfinished">Sauvegarder</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="15"/>
        <source>Add tag</source>
        <translation type="unfinished">Ajouter un tag</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="109"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="128"/>
        <source>Add Tag</source>
        <translation type="unfinished">Ajouter un tag</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="9"/>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="unfinished">ScreenPlay - Double-cliquez pour modifier vos paramètres.</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="28"/>
        <source>Open ScreenPlay</source>
        <translation type="unfinished">Ouvrir ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="36"/>
        <location filename="../qml/Common/TrayIcon.qml" line="40"/>
        <source>Mute all</source>
        <translation type="unfinished">Mettez tout en sourdine</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="45"/>
        <source>Unmute all</source>
        <translation type="unfinished">Désactiver tout</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="54"/>
        <location filename="../qml/Common/TrayIcon.qml" line="58"/>
        <source>Pause all</source>
        <translation type="unfinished">Pause de tous</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="63"/>
        <source>Play all</source>
        <translation type="unfinished">Tout jouer</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="70"/>
        <source>Quit</source>
        <translation type="unfinished">Sortie</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="63"/>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="137"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="152"/>
        <source>Upload Projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="212"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="114"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="121"/>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="141"/>
        <source>Invalid Project!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="43"/>
        <source>Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="46"/>
        <source>No Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="49"/>
        <source>Invalid Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="52"/>
        <source>Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="55"/>
        <source>Invalid Protocol Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="58"/>
        <source>Invalid Param</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="61"/>
        <source>File Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="64"/>
        <source>Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="67"/>
        <source>Invalid State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="70"/>
        <source>Invalid Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="73"/>
        <source>Invalid Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="76"/>
        <source>Duplicate Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="79"/>
        <source>Access Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="82"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="85"/>
        <source>Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="88"/>
        <source>Account Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="91"/>
        <source>Invalid SteamID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="94"/>
        <source>Service Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="97"/>
        <source>Not Logged On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="100"/>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="103"/>
        <source>Encryption Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="106"/>
        <source>Insufficient Privilege</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="109"/>
        <source>Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="112"/>
        <source>Revoked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="115"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="118"/>
        <source>Already Redeemed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="121"/>
        <source>Duplicate Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="124"/>
        <source>Already Owned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="127"/>
        <source>IP Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="130"/>
        <source>Persist Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="133"/>
        <source>Locking Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="136"/>
        <source>Logon Session Replaced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="139"/>
        <source>Connect Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="142"/>
        <source>Handshake Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="145"/>
        <source>IO Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="148"/>
        <source>Remote Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="151"/>
        <source>Shopping Cart Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="154"/>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="157"/>
        <source>Ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="160"/>
        <source>No Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="163"/>
        <source>Account Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="166"/>
        <source>Service ReadOnly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="169"/>
        <source>Account Not Featured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="172"/>
        <source>Administrator OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="175"/>
        <source>Content Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="178"/>
        <source>Try Another CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="181"/>
        <source>Password Required T oKick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="184"/>
        <source>Already Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="187"/>
        <source>Suspended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="190"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="193"/>
        <source>Data Corruption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="196"/>
        <source>Disk Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="199"/>
        <source>Remote Call Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="202"/>
        <source>Password Unset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="205"/>
        <source>External Account Unlinked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="208"/>
        <source>PSN Ticket Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="211"/>
        <source>External Account Already Linked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="214"/>
        <source>Remote File Conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="217"/>
        <source>Illegal Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="220"/>
        <source>Same As Previous Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="223"/>
        <source>Account Logon Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="226"/>
        <source>Cannot Use Old Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="229"/>
        <source>Invalid Login AuthCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="232"/>
        <source>Account Logon Denied No Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="235"/>
        <source>Hardware Not Capable Of IPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="238"/>
        <source>IPT Init Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="241"/>
        <source>Parental Control Restricted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="244"/>
        <source>Facebook Query Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="247"/>
        <source>Expired Login Auth Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="250"/>
        <source>IP Login Restriction Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="253"/>
        <source>Account Locked Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="256"/>
        <source>Account Logon Denied Verified Email Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="259"/>
        <source>No MatchingURL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="262"/>
        <source>Bad Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="265"/>
        <source>Require Password ReEntry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="268"/>
        <source>Value Out Of Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="271"/>
        <source>Unexpecte Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="274"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="277"/>
        <source>Invalid CEG Submission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="280"/>
        <source>Restricted Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="283"/>
        <source>Region Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="286"/>
        <source>Rate Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="289"/>
        <source>Account Login Denied Need Two Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="292"/>
        <source>Item Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="295"/>
        <source>Account Login Denied Throttle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="298"/>
        <source>Two Factor Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="301"/>
        <source>Two Factor Activation Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="304"/>
        <source>Account Associated To Multiple Partners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="307"/>
        <source>Not Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="310"/>
        <source>No Mobile Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="313"/>
        <source>Time Not Synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="316"/>
        <source>Sms Code Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="319"/>
        <source>Account Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="322"/>
        <source>Account Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="325"/>
        <source>Phone Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="328"/>
        <source>Refund To Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="331"/>
        <source>Email Send Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="334"/>
        <source>Not Settled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="337"/>
        <source>Need Captcha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="340"/>
        <source>GSLT Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="343"/>
        <source>GS Owner Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="346"/>
        <source>Invalid Item Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="349"/>
        <source>IP Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="352"/>
        <source>GSLT Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="355"/>
        <source>Insufficient Funds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="358"/>
        <source>Too Many Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="361"/>
        <source>No Site Licenses Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="364"/>
        <source>WG Network Send Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="367"/>
        <source>Account Not Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="370"/>
        <source>Limited User Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="373"/>
        <source>Cant Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="376"/>
        <source>Account Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="379"/>
        <source>Existing User Cancelled License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="382"/>
        <source>Community Cooldown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="446"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="463"/>
        <source>Upload Progress: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Workshop</name>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="151"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="162"/>
        <source>Download now!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="167"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="174"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="198"/>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="260"/>
        <source>Search for Wallpaper and Widgets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="296"/>
        <source>Ranked By Vote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="299"/>
        <source>Publication Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="302"/>
        <source>Ranked By Trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="305"/>
        <source>Favorited By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="308"/>
        <source>Created By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="311"/>
        <source>Created By Followed Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="314"/>
        <source>Not Yet Rated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="317"/>
        <source>Total VotesAsc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="320"/>
        <source>Votes Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Workshop.qml" line="323"/>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="185"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="303"/>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="413"/>
        <source>Download complete!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <location filename="../qml/Community/XMLNewsfeed.qml" line="53"/>
        <source>News &amp; Patchnotes</source>
        <translation type="unfinished">Nouveautées et notes de versions</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="obsolete">ScreenPlay - Double-cliquez pour modifier vos paramètres.</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation type="obsolete">Ouvrir ScreenPlay</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation type="obsolete">Mettez tout en sourdine</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation type="obsolete">Désactiver tout</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation type="obsolete">Pause de tous</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation type="obsolete">Tout jouer</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">Sortie</translation>
    </message>
</context>
</TS>
